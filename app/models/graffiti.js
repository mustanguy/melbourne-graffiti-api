var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var GraffitiSchema   = new Schema({
    name: String,
    lat: String,
    lng: String,
    photo: String,
    description: String,
    artist: String,
    status: Boolean,
    date: Date,

});

module.exports = mongoose.model('Graffiti', GraffitiSchema);
