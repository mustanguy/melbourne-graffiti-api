// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var cors = require('cors');
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
app.use(cors());

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// BDD info
var mongoose = require('mongoose');
mongoose.connect('mongodb://root:melbourneapi@localhost:27017/melbourne', { useNewUrlParser: true, useUnifiedTopology: true }); // connect to our database

// link models
var Graffiti = require('./app/models/graffiti');

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

var path = require('path');

app.use(express.static('public'));

var multer  = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, './public/images');
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + '.jpeg');
    }
});
var upload = multer({storage: storage});

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

// more routes for our API will happen here
router.route('/graffiti')

    // create a graffiti (accessed at POST http://localhost:8080/api/graffitis)
    .post(upload.single('file'), function(req, res, next) {
      const file = req.file
      if (!file) {
       const error = new Error('Please upload a file')
       error.httpStatusCode = 400
       return next(error)
      }
      //res.send(file)

      var graffiti = new Graffiti();      // create a new instance of the graffiti model
      var today = new Date();
      graffiti.name = req.body.name;
      graffiti.artist = req.body.artist;
      graffiti.description = req.body.description;
      graffiti.lat = req.body.lat;
      graffiti.lng = req.body.lng;
      graffiti.status = 0;
      graffiti.date = today;
      graffiti.photo = "http://139.99.97.36:8080/images/" + file.filename;


      graffiti.save(function(err) {
          if (err)
              res.send(err);

          res.json(file);
      });



    }).get(function(req, res) {
        Graffiti.find(function(err, graffitis) {
            if (err)
                res.send(err);

            res.json(graffitis);
          });
    });

router.route('/graffiti/random').get(function(req, res) {
    Graffiti.count().exec(function (err, count) {

        // Get a random entry
        var random = Math.floor(Math.random() * count);

        // Again query all users but only fetch one offset by our random #
        Graffiti.findOne().skip(random).exec(
            function (err, graffiti) {
                res.json(graffiti);
            })
    });
});
// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
